var $t = jQuery.noConflict();

$t(function($){
	$('.sc_email_to_download').submit(function(e){
		e.preventDefault();
		var form = $(this);
		$.post(sc_email_to_download.ajax_url, $(this).serialize(), function(response){
			var respuesta = JSON.parse(response);
			console.log(respuesta.code);
			if(respuesta.code === 202){
				form.html(respuesta.msg);
			}
		});
	})
});