<?php
/*
Plugin Name: SC Email to Download
Plugin URI: https://smartcodes.xyz
Description: 
Author: ArturoGallegos
Version: 1.0
Author URI: https://smartcodes.xyz
*/

/*
CREATE TABLE `wordpress`.`sc_email_to_download` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , `email` VARCHAR(255) NOT NULL , `file_id` INT NOT NULL , `token` VARCHAR(255) NOT NULL , `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB; 
*/
class sc_email_to_download{
	public function init(){
		add_shortcode('sc_email_to_download', [$this, 'shortcode']);
		add_action('wp_ajax_sc_email_to_download', [$this, 'save']);
		add_action('wp_ajax_nopriv_sc_email_to_download', [$this, 'save']);
		add_action('init', [$this, 'download_file'], 1);
		add_filter('media_buttons_context', [$this, 'add_btn_editor_condor']);
	}

	public function add_btn_editor(){
		?><span class="button button-primary" id="agregar_x_0892189389231">Add downloadable file</span>
<script>
	var frame, $t = jQuery.noConflict();
	$t(function($){
		$('#agregar_x_0892189389231').click(function(){
			frame = wp.media({
	      title: 'Select file to download',
	      button: {
	        text: 'Use this'
	      },
	      multiple: true
	    });
	    frame.on('select', function(){
	      var attachment = frame.state().get('selection').first().toJSON();
	      wp.media.editor.insert('[sc_email_to_download file_id="'+attachment.id+'" /]');
	    });
	    frame.open();
		});
	});
</script>
		<?php
	}

	public function shortcode($atts){
		$atts_x = shortcode_atts(['file_id' => 1], $atts);
		$form_id = $atts_x['file_id'] . rand() . rand();
		wp_enqueue_style('sc_email_to_download', plugins_url('/sc_email_to_download.css', __FILE__), [], null);
		wp_enqueue_script('sc_email_to_download', plugins_url('/sc_email_to_download.min.js', __FILE__), ['jquery'], null, true);
		wp_localize_script('sc_email_to_download', 'sc_email_to_download', ['ajax_url' => admin_url('admin-ajax.php')]);
		ob_start();
		?>
<form action="<?php echo get_the_permalink(); ?>" method="post" class="sc_email_to_download">
	<input type="hidden" name="action" value="sc_email_to_download">
	<input type="hidden" name="file_id" value="<?php echo $atts_x['file_id']; ?>">
	<label for="field_name<?php echo $form_id; ?>">Nombre *</label>
	<input type="text" name="name" id="field_name<?php echo $form_id; ?>">
	<label for="field_email<?php echo $form_id; ?>">Email *</label>
	<input type="text" name="email" id="field_name<?php echo $form_id; ?>">
	<label><input type="checkbox" required=""> Aceptar terminos y condiciones</label>
	<div><button>Descargar</button></div>
</form>
		<?php
		return ob_get_clean();
	}

	public function save(){
		if(empty($_POST['email']) && empty($_POST['file_id'])){
			echo json_encode(['code' => 404]);
			die();
		}
		global $wpdb;
		$token = $wpdb->get_var('select token from sc_email_to_download where email = "'.$_POST['email'].'" and file_id = '.$_POST['file_id']);
		if(empty($token)){
			$token = md5($_POST['email'].$_POST['file_id']);
			$wpdb->insert('sc_email_to_download', array(
				'name' => $_POST['name'],
				'email' => $_POST['email'],
				'file_id' => $_POST['file_id'],
				'token' => $token
			));
		}
		ob_start();
		?>
Gracias, ahora puedes descargar tu archivo dando click en el siguiente enlace<br>
<a target="_blank" href="<?php echo site_url() . '?sc_email_to_download='.$token; ?>">Descargar</a>
		<?php
		$msg = ob_get_clean();
		echo json_encode(['code' => 202, 'url' => site_url() . '?sc_email_to_download='.$token, 'msg' => $msg]);
		die();
	}

	public function download_file(){
		if(isset($_GET['sc_email_to_download']) && !empty($_GET['sc_email_to_download'])){
			global $wpdb;
			$file_id = $wpdb->get_var('select file_id from sc_email_to_download where token = "'.$_GET['sc_email_to_download'].'"');
			if(empty($file_id)){
				wp_safe_redirect(site_url());
				die();
			}
			$file_url = wp_get_attachment_url($file_id);
			header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary"); 
			header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
			readfile($file_url);
			die();
		}
	}
}

$init_sc_email_to_download = new sc_email_to_download();
$init_sc_email_to_download->init();

include_once __DIR__ . '/sc_email_to_download_admin.php';