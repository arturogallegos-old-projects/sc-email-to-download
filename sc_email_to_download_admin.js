new Vue({
	el:'#sc_email_to_download_admin',
	data:{
		emails:{
			data:[],
			total_pages:1,
			current_page:1,
		}
	},
	mounted(){
		this.get_data(1);
	},
	computed:{
    pagination(){
      var this_ = this, items = [], current_page = parseInt(this_.emails.current_page), total_pages = parseInt(this_.emails.total_pages);
      if(total_pages < 10){
        for(var n = 0; n < total_pages; n++){
          items.push({
            page:(n+1),
            status:(n+1) === current_page,
          });
        }
      }else{
          if(current_page < 6){
            for(var n = 0; n < 10; n++){
              items.push({
                page:(n+1),
                status:(n+1) === current_page,
              });
            }
            items.push({
              page:'...',
              status:false
            });
          }
          else if(current_page >= (total_pages - 10)){
          	items.push({
              page:'...',
              status:false
            });
            for(var n = (total_pages - 11); n < total_pages; n++){
              items.push({
                page:(n+1),
                status:(n+1) === current_page,
              });
            }
          }
          else{
            items.push({
              page:'...',
              status:false
            });
            for(var n = current_page - 5; n < current_page; n++){
              items.push({
                page:(n+1),
                status:(n+1) === current_page,
              });
            }
            for(var n = current_page; n < current_page + 5; n++){
              items.push({
                page:(n+1),
                status:(n+1) === current_page,
              });
            }
            items.push({
              page:'...',
              status:false
            });
          }
      }
      return items;
    }
  },
	methods:{
		get_data(page){
			var this_ = this, formData = new FormData();
			formData.append('action', 'sc_email_to_download_admin');
			formData.append('paged', page);
			axios.post(sc_email_to_download_admin.ajax_url, formData).then(function(response){
				console.log(response.data);
				this_.emails = response.data;
			});
		},
	}
});