<?php
class sc_email_to_download_admin{
	public function init(){
		add_action('admin_menu', [$this, 'add_pages']);
		add_action('wp_ajax_sc_email_to_download_admin', [$this, 'get_data']);
	}

	public function add_pages(){
		add_menu_page('SC Email to download admin', 'Email to Download', 'manage_options', 'sc_email_to_download_admin', [$this, 'html']);
	}

	public function html(){
		?>
<div id="sc_email_to_download_admin">
	<ul class="pagination">
      <li v-for="item in pagination" @click="get_data(item.page)" :class="{active:item.status}">{{ item.page }}</li>
  </ul>
  <!-- <pre>{{ emails.total_pages }}</pre> -->
	<table>
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Email</th>
				<th>File ID</th>
				<th>Fecha</th>
			</tr>
		</thead>
		<tbody>
			<tr v-for="item in emails.data">
				<td>{{ item.id }}</td>
				<td>{{ item.name }}</td>
				<td>{{ item.email }}</td>
				<td><a :href="'<?php echo site_url() ?>?p='+item.file_id" target="_blank">Archivo {{ item.file_id }}</a></td>
				<td>{{ item.date }}</td>
			</tr>
		</tbody>
	</table>
</div>
		<?php
		wp_enqueue_style('sc_email_to_download_admin', plugins_url('/sc_email_to_download_admin.css', __FILE__), [], null);
		wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js', [], null, true);
		wp_enqueue_script('vue', 'https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.min.js', [], null, true);
		wp_enqueue_script('sc_email_to_download_admin', plugins_url('/sc_email_to_download_admin.js', __FILE__), ['vue'], null, true);
		wp_localize_script('sc_email_to_download_admin', 'sc_email_to_download_admin', ['ajax_url' => admin_url('admin-ajax.php')]);
	}

	public function get_data(){
		global $wpdb;
		$limit_start = $_POST['paged'] > 1 ? (($_POST['paged'] - 1) * 100) : 0;
		$limit_end = $limit_start + 99;
		$emails = $wpdb->get_results('select * from sc_email_to_download limit '.$limit_start.', 100');
		$total_rows = $wpdb->get_var('select count(id) from sc_email_to_download');
		echo json_encode(array(
			'post' => $_POST,
			'total_pages' => ceil($total_rows/100),
			'current_page' => isset($_POST['paged']) ? $_POST['paged'] : 1,
			'data' => $emails,
		));
		die();
	}

	public function fake(){
		global $wpdb;
		for($i=501;$i<2000;$i++){
			$wpdb->insert('sc_email_to_download',array(
				'name' => 'Demo '.$i,
				'email' => 'demo'.$i.'@demo.com',
				'file_id' => 94,
				'token' => md5('94demo'.$i.'@demo.com')
			));
		}
	}
}
$init_sc_email_to_download_admin = new sc_email_to_download_admin();
$init_sc_email_to_download_admin->init();